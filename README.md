# Why Haskell

In short, I picked Haskell because of the undergrad project.

Now to put it in more words. I have spent almost five years working on a relatively high-level integration and mangement systems with soft realtime requirements. Without specifics, I can only say that I believe I have seen what C++ software and general system devleopment could have looked like 30-40 years ago, from the code base with heisenbugs, to the burned out apathetic cadre pool, to decision making processes. I cannot say I am warm to what I have seen, yet I still want to do better than that. For that, C++11 development and 40-year old architectural practices and the complete lack of those is insufficient. 

NITTA project caught my attention because I have perceived it as a way of broadening my perspective. I believe working on and understanding a CAD for synthesizing ASIC is an important practical waypoint towards building up a more complex, more systemic view of system development. Considering that NITTA is implemented in a completely different paradigm, I believe practical experience with it will be useful in C++ programming practice as well. I cannot yet say where this path will lead me, but I trust my intuition here. It hasn't yet failed me.

Owing to NITTA core being implemented in Haskell, I have decided to pick it for the course. I shall employ the following set of development instruments:

- GHC 9.2.7 as a compiler;
- Stack as a package manager; 
- hlint as a linter;
- fourmolu for automated code formatting;
- Sublime Text for writing code.

From the variety of books presented in the Haskell Wiki (https://wiki.haskell.org/Books), I have settled on Bragilevsky's _Haskell in Depth_ as the main choice; however, as it was stated to be "the perfect second book", I have decided to skim (but not thoroughly work) through Kurt's _Get Programming with Haskell_ first, without any prior knowledge.
